Feature: Test a query from yahoo website

  Scenario: Get celebrity API testing
    Given I have set the headers for the request
    When I send the get request
    Then I should see 200 as response

  Scenario: Read the input from json file
    Given I have set the headers for the request
    When I set the request body from the file

  Scenario: Sample json processing from a response
    Given I make a request to retrieve a json
    When I send the request
    And I check the UserAgent being returned

package com.automation.backend.requests;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class GetJsonRequest extends BaseRequests {


    public void setHeaders() {
        // httpRequest.header("")
        setEnv();
    }

    public void setEnv() {
        httpRequest.baseUri("http://headers.jsontest.com/");
    }

    public void sendRequest() {
        httpResponse = httpRequest.get();
        httpResponse.prettyPrint();
    }

    public boolean checkResponse(String s) {
        //  httpResponse.jsonPath().getString("result.status").equals("")
        return httpResponse.jsonPath().getString(s).equals("Apache-HttpClient/4.5.3 (Java/1.8.0_161)");
        //actualTransactionMap.put("completedTimestamp", fromResponse.getBody().jsonPath().getJsonObject(COMPLETED_TIMESTAMP_STRING));
    }

    //        setUserNameInUse(arg0 + new Date().getTime());
}

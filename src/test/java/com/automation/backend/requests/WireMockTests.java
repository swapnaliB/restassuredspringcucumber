package com.automation.backend.requests;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.junit.Rule;
import org.junit.Test;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static org.junit.Assert.assertEquals;


public class WireMockTests {

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(8089);

    RequestSpecification httpRequest;

    Response httpResponse;

    @Test
    public void sampleTest() {

        Header header1 = new Header("ContentType", "application/json");
        Header header2 = new Header("ContentType", "text/plain");


        stubFor(get("/consumer/subscriber").withHeader("ContentType", equalTo("application/json")).willReturn(aResponse().withStatus(200)));
        stubFor(get("/consumer/alias").withHeader("ContentType", equalTo("application/json")).willReturn(aResponse().withStatus(400)));

        httpRequest = RestAssured.given().header("ContentType", "application/json");

        httpResponse = httpRequest.get("http://127.0.0.1:8089/consumer/subscriber");
        assertEquals(httpResponse.getStatusCode(), 200);

        httpResponse = RestAssured.given().
                header(header1).get("http://127.0.0.1:8089/consumer/alias");
        assertEquals(httpResponse.getStatusCode(), 400);

        httpResponse = RestAssured.given().
                header(header1).get("http://127.0.0.1:8089/consumer/alias");
        assertEquals(httpResponse.getStatusCode(), 400);

        httpResponse = RestAssured.given().
                header(header2).get("http://127.0.0.1:8089/something");
        assertEquals(httpResponse.getStatusCode(), 200);
    }
}

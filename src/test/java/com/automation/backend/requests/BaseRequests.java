package com.automation.backend.requests;

import io.restassured.RestAssured;
import io.restassured.config.SSLConfig;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class BaseRequests {
    RequestSpecification httpRequest;
    Response httpResponse;

    protected BaseRequests() {
        httpRequest = RestAssured.given().config(RestAssured.config().sslConfig(new SSLConfig().relaxedHTTPSValidation("SSL")));
    }

    public boolean checkResponse(int i) {
        return httpResponse.getStatusCode() == i;
    }

    public abstract void setHeaders();

    public abstract void setEnv();
}

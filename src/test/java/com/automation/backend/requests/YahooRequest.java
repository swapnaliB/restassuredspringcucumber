package com.automation.backend.requests;

import com.automation.backend.requests.BaseRequests;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.HashMap;

import static org.apache.http.HttpHeaders.ACCEPT;
import static org.apache.http.HttpHeaders.CONTENT_TYPE;

@Component
@Slf4j
public class YahooRequest extends BaseRequests {

    public void setHeaders() {
        setEnv();
        httpRequest.header(CONTENT_TYPE, "application/json; charset=UTF-8");
        httpRequest.header(ACCEPT, "application/json");
    }

    @Override
    public void setEnv() {
        httpRequest.baseUri("https://en.wikipedia.org");
        httpRequest.basePath("/wiki/Test");

    }

    public void sendGetRequest() {
        httpResponse = httpRequest.get();
        httpResponse.prettyPrint();
    }

    public void setBody(HashMap<String, Object> readFromFile) {
        httpRequest.body(readFromFile);
    }
}

package com.automation.backend.helper;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;

@Component
public class ReadFromJsonFile {

    public HashMap<String, Object> readFromFile(String filename) {
        HashMap<String, Object> map = null;
        try {
            String payload = new String(Files.readAllBytes(Paths.get(System.getProperty("user.dir") + "/src/test/resources/" + filename)));
            map = new Gson().fromJson(payload, new TypeToken<HashMap<String, Object>>() {
            }.getType());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return map;
        //   map.put("collectionEventMetadata", collectionEventMetaDataMap);
        //   httpRequest.body(map);
    }
}

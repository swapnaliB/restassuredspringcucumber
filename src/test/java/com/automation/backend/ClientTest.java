package com.automation.backend;

import au.com.dius.pact.consumer.Pact;
import au.com.dius.pact.consumer.PactProviderRuleMk2;
import au.com.dius.pact.consumer.PactVerification;
import au.com.dius.pact.consumer.dsl.PactDslWithProvider;
import au.com.dius.pact.model.RequestResponsePact;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.junit.Rule;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ClientTest {

    @Rule
    public PactProviderRuleMk2 pactProviderRuleMk2 = new PactProviderRuleMk2("test Provider", "localhost", 8092, this);
    private Response httpResponse;

    @Pact(consumer = "test consumer")
    public RequestResponsePact createPact(PactDslWithProvider buider) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");
        return buider.given("test GET").uponReceiving("Get Request").
                path("/provider.json").method("GET").
                willRespondWith().status(200).headers(headers).toPact();
        //.body("{\"condition\" : true,\"name\":\"tom\"}").toPact();

    }

    @Test
    @PactVerification()

    public void happyPathTest() {
        httpResponse = RestAssured.given().get("http://localhost:8092/provider.json");
        assertEquals(httpResponse.getStatusCode(), 200);

    }
}

package com.automation.backend.stepDefs;

import com.automation.backend.helper.ReadFromJsonFile;
import com.automation.backend.requests.GetJsonRequest;
import com.automation.backend.requests.YahooRequest;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;

import static junit.framework.TestCase.assertTrue;

public class MyStepdefs {
    @Autowired
    private YahooRequest yahooRequest;

    @Autowired
    private ReadFromJsonFile readFromJsonFile;

    @Autowired
    private GetJsonRequest getJsonRequest;

    @Given("^I have set the headers for the request$")
    public void iHaveSetTheHeadersForTheRequest() {
        yahooRequest.setHeaders();
    }

    @When("^I send the get request$")
    public void iSendTheGetRequest() {
        yahooRequest.sendGetRequest();
    }

    @Then("^I should see (\\d+) as response$")
    public void iShouldSeeAsResponse(int arg0) {
        assertTrue(yahooRequest.checkResponse(arg0));
    }

    @When("^I set the request body from the file$")
    public void iSetTheRequestBodyFromTheFile() {
        yahooRequest.setBody(readFromJsonFile.readFromFile("sdkInitPayload.json"));
    }


    @Given("^I make a request to retrieve a json$")
    public void iMakeARequestToRetrieveAJson() {
        getJsonRequest.setHeaders();
    }

    @When("^I send the request$")
    public void iSendTheRequest() {
        getJsonRequest.sendRequest();
    }

    @And("^I check the UserAgent being returned$")
    public void iCheckTheUserAgentBeingReturned() {
        assertTrue(getJsonRequest.checkResponse("User-Agent"));
    }
}
